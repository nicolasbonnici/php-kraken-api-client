# Kraken API php client

Simple Kraken API client wrote using PHP8 and [Ratchet WebSocket client Pawl](https://github.com/ratchetphp/Pawl) for the real time usage.

### Getting started

Install via composer

```bash
composer require nicolasbonnici/kraken-api-php-client
```

Usage and more info about this repository on this post: [https://dev.to/nicolasbonnici/craft-a-kraken-api-client-with-php8-12-2hc7](https://dev.to/nicolasbonnici/craft-a-kraken-api-client-with-php8-12-2hc7)
