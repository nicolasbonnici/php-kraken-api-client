<?php
namespace NicolasBonnici\PhpKrakenApiClient;

use NicolasBonnici\PhpKrakenApiClient\Exception\KrakenAPIException;

class KrakenAPIClient
{
    public const USER_AGENT = __NAMESPACE__;
    public const DEFAULT_API_ROOT = 'https://api.kraken.com';
    public const DEFAULT_API_VERSION = '0';
    public const AUTHENTICATED_ENDPOINTS_URI_PATH = 'private/';
    public const WEBSOCKET_PUBLIC_ENDPOINT = 'ws.kraken.com';
    public const WEBSOCKET_PRIVATE_ENDPOINT = 'ws-auth.kraken.com';

    public function __construct(
        private ?string $key = null,
        private ?string $secret = null,
        private string $apiRoot = self::DEFAULT_API_ROOT,
        private string $version = self::DEFAULT_API_VERSION,
        private bool $sslCheck = true,
        private bool|\CurlHandle $curl = false,
    ) {
        if (!function_exists('curl_init')) {
            throw new KrakenAPIException('No curl extension available.');
        }

        $this->loadClient();
    }

    public function __destruct()
    {
        if (function_exists('curl_close') && $this->curl instanceof \CurlHandle) {
            curl_close($this->curl);
        }
    }

    /**
     * @throws KrakenAPIException
     */
    public function query(
        string $endpoint,
        array $request = [],
        array $headers = []
    ): array {
        $authenticated = str_starts_with($endpoint, self::AUTHENTICATED_ENDPOINTS_URI_PATH);

        $uri = sprintf('/%s/%s', $this->version, $endpoint);

        if (true === $authenticated) {
            $headers = $this->signRequest($uri, $request, $headers);
        } else {
            $this->buildRequest($request);
        }

        curl_setopt($this->curl, CURLOPT_URL, $this->apiRoot . $uri);
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->curl);
        if ($response === false) {
            throw new KrakenAPIException(sprintf('CURL error: "%s"', curl_error($this->curl)));
        }

        $response = json_decode($response, true);
        if (!is_array($response)) {
            throw new KrakenAPIException(sprintf('JSON decode error: "%s"', json_last_error_msg()));
        }

        if (false === isset($response['result'])) {
            throw new KrakenAPIException(
                $response['error'] ? implode('. ', $response['error']) : 'Unknown error occur'
            );
        }

        return $response['result'];
    }

    public function stream(callable $callback, string $endpoint = self::WEBSOCKET_PUBLIC_ENDPOINT): void
    {
        \Ratchet\Client\connect(sprintf('wss://%s', $endpoint))->then(
            $callback,
            function ($e) {
                throw new KrakenAPIException(
                    sprintf('Error while connecting to Kraken API WebSocket: "%s".', $e->getMessage())
                );
            }
        );
    }

    private function loadClient(): void
    {
        if ($this->curl === false) {
            $this->curl = curl_init();
        }

        curl_setopt_array($this->curl, [
            CURLOPT_SSL_VERIFYPEER => $this->sslCheck,
            CURLOPT_SSL_VERIFYHOST => 2,
            CURLOPT_USERAGENT => self::USER_AGENT,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true
        ]);
    }

    private function signRequest(string $uri, array $request, array $headers): array
    {
        if (!$this->key || !$this->secret) {
            throw new KrakenAPIException('No API credentials, please provide both key and secret.');
        }

        if (!isset($request['nonce'])) {
            $request['nonce'] = $this->generateNonce();
        }

        $sign = hash_hmac(
            'sha512',
            $uri . hash('sha256', $request['nonce'] . $this->buildRequest($request), true),
            base64_decode($this->secret),
            true
        );

        return array_merge($headers, [
            'API-Key: ' . $this->key,
            'API-Sign: ' . base64_encode($sign)
        ]);
    }

    private function buildRequest(array $request = []): string
    {
        $httpQuery = http_build_query($request, '', '&');
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $httpQuery);

        return $httpQuery;
    }

    private function generateNonce(): string
    {
        // generate a 64 bit nonce using a timestamp at microsecond resolution
        $nonce = explode(' ', microtime());
        return $nonce[1] . str_pad(substr($nonce[0], 2, 6), 6, '0');
    }

    public function setKey(?string $key): self
    {
        $this->key = $key;

        return $this;
    }

    public function setSecret(?string $secret): self
    {
        $this->secret = $secret;

        return $this;
    }

    public function setApiRoot(string $apiRoot): self
    {
        $this->apiRoot = $apiRoot;

        return $this;
    }

    public function setVersion(string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function setSslCheck(bool $sslCheck): self
    {
        $this->sslCheck = $sslCheck;

        return $this;
    }
}
